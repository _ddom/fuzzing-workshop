LIBPNG=libpng-1.6.36
AFL=afl-2.52b
VSFTPD=vsftpd-3.0.3

LIBPNGURL=https://download.sourceforge.net/libpng/$(LIBPNG).tar.xz
AFLURL=http://lcamtuf.coredump.cx/afl/releases/afl-latest.tgz
VSFTPDURL=https://security.appspot.com/downloads/$(VSFTPD).tar.gz

LIBPNGOUT=libpng-afl-install
LIBPNGA=$(LIBPNGOUT)/lib/libpng.a

TESTS=test-libpng-afl
FUZZERS=fuzz-libpng-with-pngs fuzz-libpng-with-jpgs fuzz-vsftpd-with-boofuzz

VSFTPDTMP=`pwd`/vsftpd-fuzzing

.PHONY: clean clean-outs clean-all test $(TESTS) $(FUZZERS) build fuzzers

# By default build everything
build: $(AFL)/afl-gcc $(LIBPNGA) $(VSFTPD)/vsftpd

fuzzers:
	@echo $(FUZZERS) | tr ' ' '\n'

test: $(TESTS)

test-libpng-afl:
	$(AFL)/afl-showmap -o `mktemp` -- $(LIBPNG)/.libs/pngtest pngs/gopher.png

fuzz-libpng-with-pngs:
	mkdir -p $@-out
	cp -r pngs $@-ins
	$(AFL)/afl-fuzz -i $@-ins -o $@-out -T $@ -- $(LIBPNG)/.libs/pngtest @@

fuzz-libpng-with-jpgs:
	mkdir -p $@-out
	cp -r jpgs $@-ins
	$(AFL)/afl-fuzz -i $@-ins -o $@-out -T $@ -- $(LIBPNG)/.libs/pngtest @@

fuzz-vsftpd-with-boofuzz:
	python2 ftp-fuzzer.py

$(LIBPNGA): $(LIBPNG) $(AFL)/afl-gcc
	cd $(LIBPNG) && \
	CC=`pwd`/../$(AFL)/afl-gcc ./configure --prefix=`pwd`/../$(LIBPNGOUT) && \
	$(MAKE) check && \
	$(MAKE) install

$(AFL)/afl-gcc: $(AFL)
	$(MAKE) -C $(AFL)

$(VSFTPD)/vsftpd: $(VSFTPD)
	$(MAKE) -C $(VSFTPD)
	grep ^nobody /etc/passwd || sudo useradd nobody
	sudo mkdir -p /usr/share/empty
	sudo mkdir -p $(VSFTPDTMP)/var/ftp
	grep ^ftp /etc/passwd || sudo useradd -d $(VSFTPDTMP)/var/ftp ftp
	sudo chown root.root $(VSFTPDTMP)/var/ftp
	sudo chmod og-w $(VSFTPDTMP)/var/ftp

$(AFL): artifacts/afl-latest.tgz
	tar -xf $^

$(LIBPNG): artifacts/$(LIBPNG).tar.xz
	tar -xf $^

$(VSFTPD): artifacts/$(VSFTPD).tar.gz
	tar -xf $^

artifacts/afl-latest.tgz: artifacts
	wget $(AFLURL) -O $@

artifacts/$(LIBPNG).tar.xz: artifacts
	wget $(LIBPNGURL) -O $@

artifacts/$(VSFTPD).tar.gz: artifacts
	wget $(VSFTPDURL) -O $@

artifacts:
	mkdir -p artifacts

clean:
	rm -rf $(LIBPNG)
	rm -rf $(AFL)
	rm -rf artifacts
	rm -rf $(LIBPNGOUT)
	rm -f pngout.png
	rm -rf *-ins
	rm -rf $(VSFTPD)
	sudo rm -rf $(VSFTPDTMP)
	grep ^ftp /etc/passwd && sudo userdel ftp || true

clean-outs:
	rm -rf *-out
	rm -rf boofuzz-results

clean-all: clean clean-outs
