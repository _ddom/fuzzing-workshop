# -*- coding: utf-8 -*-

"""
A sample fuzzer for FTP based on boofuzz. Works with python2.

It has a lot of the protocol defined, both commands and relations between them.
It is missing some cases, but as a demostration of a generative fuzzer is more
than enough.

For a more usable fuzzer but with less rules defined for the FTP protocol look
for https://github.com/jtpereyda/boofuzz-ftp

Daniel Domínguez <daniel.dominguez@imdea.org>
"""

# Import the boofuzz module for the definition of the fuzzer
from boofuzz import *

# Boofuzz works with the idea of sessions. Each session must have a target.
# In this case the target is a server in tcp://localhost:21
session = Session(
    target=Target(
        connection=SocketConnection("127.0.0.1", 21, proto='tcp')))

# The fuzzer works by defining blocks. Each block has a sequence of inputs.
# Some inputs can be modified and will be modified by the fuzzer as part of
# the fuzzing process. The parts declared as static will be left as is by the
# fuzzer. This static parts can be used for thing like delimiting input in a
# protocol.
s_initialize("user")
s_string("USER")
s_delim(" ")
s_string("anonymous")
s_static("\r\n")

s_initialize("pass")
s_string("PASS")
s_delim(" ")
s_string("fuzzer")
s_static("\r\n")

s_initialize("stor")
s_string("STOR")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("retr")
s_string("RETR")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("abor")
s_string("ABOR")
s_static("\r\n")

s_initialize("acct")
s_string("ACCT")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("allo")
s_string("ALLO")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("allo2")
s_string("ALLO")
s_delim(" ")
s_string("AAAA")
s_delim(" R ")
s_string("AAAA")
s_static("\r\n")

s_initialize("appe")
s_string("APPE")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("cdup")
s_string("CDUP")
s_static("\r\n")

s_initialize("cwd")
s_string("CWD")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("dele")
s_string("DELE")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("help")
s_string("HELP")
s_static("\r\n")

s_initialize("help2")
s_string("HELP")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("list")
s_string("LIST")
s_static("\r\n")

s_initialize("list2")
s_string("LIST")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("mdtm")
s_string("MDTM")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("mkd")
s_string("MKD")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("mode")
s_string("MODE")
s_delim(" ")
s_group("modes", ["S", "B", "C"])
s_static("\r\n")

s_initialize("nlst")
s_string("NLST")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("noop")
s_string("NOOP")
s_static("\r\n")

s_initialize("pasv")
s_string("PASV")
s_static("\r\n")

s_initialize("port")
s_string("PORT")
s_delim(" ")
s_string("AAAA")
s_delim(",")
s_string("AAAA")
s_delim(",")
s_string("AAAA")
s_delim(",")
s_string("AAAA")
s_delim(",")
s_string("AAAA")
s_delim(",")
s_string("AAAA")
s_static("\r\n")

s_initialize("quit")
s_string("QUIT")
s_static("\r\n")

s_initialize("rein")
s_string("REIN")
s_static("\r\n")

s_initialize("rest")
s_string("REST")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("rmd")
s_string("RMD")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("rnfr")
s_string("RNFR")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("rnto")
s_string("RNTO")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("site")
s_string("SITE")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("size")
s_string("SIZE")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("stat")
s_string("STAT")
s_static("\r\n")

s_initialize("stat2")
s_string("STAT")
s_delim(" ")
s_string("AAAA")
s_static("\r\n")

s_initialize("stou")
s_string("STOU")
s_static("\r\n")

s_initialize("stru")
s_string("STRU")
s_delim(" ")
s_group("structs", ["F", "R", "P"])
s_static("\r\n")

s_initialize("syst")
s_string("SYST")
s_static("\r\n")

s_initialize("type")
s_string("TYPE")
s_delim(" ")
s_group("tc1", ["A", "E", "I", "L"])
s_delim(" ")
s_group("tc2", ["N", "T", "C"])
s_static("\r\n")

# Once we have the definition we can relate them in a graph.
# This graph is defined with python code, so, we can use loops and
# other programming constructs to define the graph. There is no need to
# define it literally.
session.connect(s_get("user"))
session.connect(s_get("user"), s_get("pass"))
session.connect(s_get("pass"), s_get("quit"))
session.connect(s_get("port"), s_get("list"))
session.connect(s_get("port"), s_get("list2"))
session.connect(s_get("pasv"), s_get("list"))
session.connect(s_get("pasv"), s_get("list2"))
session.connect(s_get("list"), s_get("quit"))
session.connect(s_get("list2"), s_get("quit"))

session_cmds = [
    "stor", "retr", "abor", "acct", "allo", "allo2", "appe", "cdup",
    "cwd", "dele", "help", "help2", "mdtm", "mkd",
    "mode", "nlst", "noop", "pasv", "port", "rein", "rest", "rmd",
    "rnfr", "site", "size", "stat", "stat2", "stou", "stru", "syst",
    "type"
]

for cmd in session_cmds:
    # All this commands happen after login
    session.connect(s_get("pass"), s_get(cmd))

    # This causes recursive definitions
    #for cmd2 in session_cmds:
    #    session.connect(s_get(cmd), s_get(cmd2))
    
    # And can be followed by a quit
    session.connect(s_get(cmd), s_get("quit"))

# Once the fuzzer is defined we can start the fuzzing process.
session.fuzz()
